let img = [];

img[0] = {
    path: "img/celnost.jpeg",
    name: "Цельность (сингл)"
}

img[1] = {
    path: "img/beregi.jpeg",
    name: "Берегi"
}

img[2] = {
    path: "img/mart.jpeg",
    name: "На границе марта"
}

img[3] = {
    path: "img/noch.jpeg",
    name: "Ночь (сингл)"
}

img[4] = {
    path: "img/planet.jpeg",
    name: "Парад планет"
}

img[5] = {
    path: "img/cvetov.jpeg",
    name: "Парад цветов"
}

let num = 0;

window.onload = function() {
    let sliderImg = document.getElementById('sliderImg');   
    sliderImg.src = img[num].path;
    picName.innerText = img[num].name;
};

function previousImg() {
    sliderImg = document.getElementById('sliderImg');
    num--;

    if (num < 0) {
        num = img.length - 1;
    }
    sliderImg.src = img[num].path;
    sliderImg.alt = img[num].name;
    picName.innerText = img[num].name;
}

function nextImg() {
    let sliderImg = document.getElementById('sliderImg');
    num++;
    num %= img.length;
    sliderImg.src = img[num].path;
    sliderImg.alt = img[num].name;
    picName.innerText = img[num].name;

}

function Comment() {

    let commentAuthor = document.getElementById('name').value;
    let commentText = document.getElementById('comment').value;
    
    let Comments = document.getElementById('Comments');
    let newComment = document.createElement('li');
    let newCommentText = commentAuthor+commentText;
    newCommentText = document.createTextNode(newCommentText);

    newComment.appendChild(newCommentText);
    Comments.appendChild(newComment);

    // alert(commentAuthor);
}

