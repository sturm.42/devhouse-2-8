
let canvas;
let context;

window.onload = function () {
    canvas = document.getElementById('drawingCanvas');
    context = canvas.getContext('2d');

    canvas.onmousedown = startDrawing;
    canvas.onmouseup = stopDrawing;
    canvas.onmouseout = stopDrawing;
    canvas.onmousemove = draw;
};

function changeThickness (thickness) {
    context.lineWidth = thickness;
};

let isDrawing;
    
function startDrawing(e) {
    isDrawing = true;
    context.beginPath();
    context.moveTo(e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop);
};

function draw(e) {
    if (isDrawing == true) {
        let x = e.pageX - canvas.offsetLeft;
        let y = e.pageY - canvas.offsetTop;
        context.lineTo(x, y);
        context.strokeStyle = "black";
        context.stroke();
    }
};

function stopDrawing () {
    isDrawing = false;
};

function clearBox() {
    context.clearRect(0, 0, canvas.width, canvas.height);
};

// function clearCanvas() {
// 	context.clearRect(0, 0, canvas.width, canvas.height);
// }